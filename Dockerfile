FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

WORKDIR /app

RUN apt-get update && apt-get install -y \
    cmake \
    gcc \
    g++ \
    git  \
    grpc++ \
    libfuse3-dev \
    libssl-dev \
    pkg-config \
    uuid-dev \
    && apt-get clean

COPY . /buildbox/

WORKDIR /buildbox

RUN mkdir build && cd build && cmake .. && make && cp buildbox-fuse /usr/bin/ && buildbox-fuse --help
